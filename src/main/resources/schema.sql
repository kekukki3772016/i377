DROP SCHEMA public CASCADE;

CREATE SEQUENCE seq1
  AS INTEGER
    START WITH 1;

CREATE TABLE customer (
  id         BIGINT NOT NULL PRIMARY KEY,
  first_name VARCHAR(255),
  last_name  VARCHAR(255),
  code       VARCHAR(100),
  type       VARCHAR(100)
);

CREATE TABLE phone (
  id          BIGINT NOT NULL PRIMARY KEY,
  type        VARCHAR(255),
  value       VARCHAR(255),
  customer_id BIGINT NOT NULL,
  FOREIGN KEY (customer_id)
  REFERENCES customer
    ON DELETE CASCADE
);

CREATE TABLE classifier (
  id   BIGINT       NOT NULL PRIMARY KEY,
  type VARCHAR(255) NOT NULL,
  UNIQUE (type)
);

CREATE TABLE classifier_value (
  id            BIGINT       NOT NULL PRIMARY KEY,
  name          VARCHAR(255) NOT NULL,
  value         VARCHAR(255) NOT NULL,
  classifier_id BIGINT       NOT NULL,
  FOREIGN KEY (classifier_id)
  REFERENCES classifier
    ON DELETE CASCADE
);


INSERT INTO customer (id, first_name, last_name, code, type)
VALUES (NEXT VALUE FOR seq1, 'Jack', 'Reacher', 123, 'customer_type.private');
INSERT INTO customer (id, first_name, last_name, code, type)
VALUES (NEXT VALUE FOR seq1, 'Jane', 'Red', 456, 'customer_type.corporate');

INSERT INTO phone (id, customer_id, type, value)
VALUES (NEXT VALUE FOR seq1, 1, 'phone_type.mobile', '123');
INSERT INTO phone (id, customer_id, type, value)
VALUES (NEXT VALUE FOR seq1, 2, 'phone_type.fixed', '456');
INSERT INTO phone (id, customer_id, type, value)
VALUES (NEXT VALUE FOR seq1, 2, 'phone_type.mobile', '789');

INSERT INTO classifier (id, type) VALUES (NEXT VALUE FOR seq1, 'customer_type');
INSERT INTO classifier (id, type) VALUES (NEXT VALUE FOR seq1, 'phone_type');

INSERT INTO classifier_value (id, name, value, classifier_id)
VALUES (NEXT VALUE FOR seq1, 'private', 123, 6);
INSERT INTO classifier_value (id, name, value, classifier_id)
VALUES (NEXT VALUE FOR seq1, 'corporate', 123, 6);
INSERT INTO classifier_value (id, name, value, classifier_id)
VALUES (NEXT VALUE FOR seq1, 'fixed', 123, 7);
INSERT INTO classifier_value (id, name, value, classifier_id)
VALUES (NEXT VALUE FOR seq1, 'mobile', 123, 7);

