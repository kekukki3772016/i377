(function(){
    'use strict';

    var app = angular.module('app');

    app.controller('EditCtrl', ctrl);

    function ctrl($http, $location, $routeParams, types){

        var vm = this;
        vm.id = undefined;
        vm.firstName = undefined;
        vm.lastName = undefined;
        vm.code = undefined;
        vm.type = undefined;
        vm.phones = [];
        vm.errors = [];
        vm.customerTypes = types.data.customerTypes;
        vm.phoneTypes = types.data.phoneTypes;
        vm.addNew = addNew;
        vm.addPhone = addPhone;
        vm.removePhone = removePhone;

        init();

        function init(){
            if($routeParams.id == undefined) {
                return;
            }

            $http.get('http://localhost:8080/api/customers/' + $routeParams.id).then(function(result){
                vm.id = result.data.id;
                vm.firstName = result.data.firstName;
                vm.lastName = result.data.lastName;
                vm.code = result.data.code;
                vm.type = result.data.type;
                vm.phones = result.data.phones;
            });
        }

        function addNew(){

            var newPost = {
                firstName : vm.firstName,
                lastName : vm.lastName,
                code : vm.code,
                type : vm.type,
                phones : vm.phones
            }

            if(vm.id !== undefined) {
                newPost.id = vm.id;
            }

            $http.post('http://localhost:8080/api/customers', newPost)
                .then(function(){
                    $location.path('/search');
                })
                .catch(function(response){
                    vm.errors = response.data.errors;
                });
        }


        function addPhone(){
            vm.phones.push({
                type : '',
                value : ''
            });
        }

        function removePhone(arrayIndex){
            if (arrayIndex > -1) {
                vm.phones.splice(arrayIndex, 1);
            }
        }

    }

})();