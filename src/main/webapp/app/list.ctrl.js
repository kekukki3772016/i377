(function(){
    'use strict';

    var app = angular.module('app');

    app.controller('ListCtrl', ctrl);

    function ctrl($http){
        var vm = this;
        vm.customers = [];
        vm.getCustomers = getCustomers;
        vm.deleteCustomer = deleteCustomer;

        init();

        function init(){
            getCustomers();
        }

        function getCustomers(){
            $http.get('http://localhost:8080/api/customers').then(function(result){
                vm.customers = result.data;
            });
        }

        function deleteCustomer(id){
            $http.delete('http://localhost:8080/api/customers/' + id).then(function(result){
                getCustomers();
            });
        }

    }

})();