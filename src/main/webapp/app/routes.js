(function () {
    'use strict';

    angular.module('app').config(conf);

    function conf($routeProvider){

        $routeProvider.when('/search', {
            templateUrl : 'app/list.html',
            controller : 'ListCtrl',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'app/edit.html',
            controller : 'EditCtrl',
            controllerAs : 'vm',
            resolve : {
                types : function($http){
                    return $http.get('http://localhost:8080/api/classifiers');
                }
            }
        }).when('/edit/:id', {
            templateUrl : 'app/edit.html',
            controller : 'EditCtrl',
            controllerAs : 'vm',
            resolve : {
                types : function($http){
                    return $http.get('http://localhost:8080/api/classifiers');
                }
            }
        }).otherwise('/search');

    }

})();