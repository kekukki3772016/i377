package dto;

import dao.PhoneDao;
import lombok.Data;
import model.Customer;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomerDto {

    private String firstName;
    private String lastName;
    private String code;
    private String type;
    private List<PhoneDto> phones = new ArrayList<>();

    public CustomerDto(Customer customer) {
        this.firstName = customer.getFirstName();
        this.lastName = customer.getLastName();
        this.code = customer.getCode();
        this.type = customer.getType();
        this.phones = new PhoneDao().toDto(customer.getPhones());
    }
}
