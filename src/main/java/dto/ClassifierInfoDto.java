package dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class ClassifierInfoDto {

    private List<String> customerTypes = new ArrayList<>();
    private List<String> phoneTypes = new ArrayList<>();

}
