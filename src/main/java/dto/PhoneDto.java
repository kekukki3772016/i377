package dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import model.Phone;

@Data
@AllArgsConstructor
public class PhoneDto {

    private String type;
    private String value;

    public PhoneDto(Phone phone) {
        this.type = phone.getType();
        this.value = phone.getValue();
    }
}
