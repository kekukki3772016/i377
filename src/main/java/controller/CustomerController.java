package controller;

import dao.CustomerDao;
import model.Customer;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CustomerController {

    @Resource
    private CustomerDao dao;

    @GetMapping(value = "customers")
    public List<Customer> getCustomers(){ return dao.getAll(); }

    @GetMapping(value = "customers/{id}")
    public Customer getCustomer(@PathVariable Long id){
        return dao.getById(id);
    }

    @GetMapping("/customers/search")
    public List<Customer> search(
            @RequestParam(defaultValue = "") String key) {
        return dao.search(key);
    }

    @PostMapping(value = "customers")
    public void saveCustomer(@RequestBody @Valid Customer customer){ dao.save(customer); }

    @DeleteMapping(value = "customers")
    public void deleteAll() { dao.deleteAll(); }

    @DeleteMapping(value = "customers/{id}")
    public void deleteCustomer(@PathVariable Long id){ dao.deleteById(id); }

}
