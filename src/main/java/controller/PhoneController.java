package controller;

import dao.PhoneDao;
import model.Phone;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class PhoneController {

    @Resource
    private PhoneDao dao;

    @GetMapping(value = "/phones")
    public List<Phone> getPhones(){ return dao.getAll(); }

    @GetMapping(value = "phones/{id}")
    public Phone getPhone(@PathVariable Long id){
        return dao.getById(id);
    }

    @DeleteMapping(value = "phones/{id}")
    public void deletePhone(@PathVariable Long id){ dao.deleteById(id); }

}
