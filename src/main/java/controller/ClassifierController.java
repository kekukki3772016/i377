package controller;

import dao.ClassifierDao;
import dao.ClassifierInfoDao;
import dao.ClassifierValueDao;
import dto.ClassifierInfoDto;
import model.Classifier;
import model.ClassifierValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("classifiers")
public class ClassifierController {

    @Resource
    private ClassifierDao cDao;

    @Resource
    private ClassifierValueDao cvDao;

    @Resource
    private ClassifierInfoDao ciDao;

    @GetMapping
    public ClassifierInfoDto getClassifiers(){
        return ciDao.getAll();
    }

    @GetMapping(value = "model")
    public List<Classifier> getClassifiersModel(){ return cDao.getAll(); }

    @GetMapping(value = "values")
    public List<ClassifierValue> getClassifierValues(){ return cvDao.getAll(); }

}
