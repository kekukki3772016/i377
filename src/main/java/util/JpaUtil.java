package util;

import configuration.JpaConfig;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class JpaUtil {

    private static EntityManagerFactory emf;

    public static EntityManagerFactory getFactory() {
        if (emf != null) {
            return emf;
        }

        emf = new JpaConfig().entityManagerFactory();

        return emf;
    }

    public static void closeFactory() {
        if (emf != null) {
            emf.close();
        }
        org.hsqldb.DatabaseManager.closeDatabases(3);
    }

    public static void closeQuietly(EntityManager em) {
        if (em != null) {
            em.close();
        }
    }

}
