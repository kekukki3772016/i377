package dao;

import dto.PhoneDto;
import model.Phone;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PhoneDao {

    @PersistenceContext
    private EntityManager em;

    public List<Phone> getAll(){
        return em.createQuery("select p from Phone p", Phone.class)
                .getResultList();
    }

    public Phone getById(Long id){
        return em.createQuery("select p from Phone p where p.id = :id", Phone.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public List<PhoneDto> toDto(List<Phone> phones){
        List<PhoneDto> phoneDtoList = new ArrayList<>();
        for (Phone phone : phones) {
            phoneDtoList.add(new PhoneDto(phone));
        }
        return phoneDtoList;
    }

    public List<Phone> getByFk(Long fk){
        return em.createQuery("select p from Phone p where p.customer_id = :fk", Phone.class)
                .setParameter("fk", fk)
                .getResultList();
    }

    public List<PhoneDto> getDtoByFk(Long fk){
        List<PhoneDto> phoneDtoList = new ArrayList<>();
        for (Phone phone : getByFk(fk)) {
            phoneDtoList.add(new PhoneDto(phone));
        }
        return phoneDtoList;
    }

    @Transactional
    public void deleteById(Long id){
        Phone phone = em.find(Phone.class, id);
        if (phone != null) {
            em.remove(phone);
        }
    }

    @Transactional
    public void deleteByFk(Long fk){
        em.createQuery("delete from Phone p where p.customer_id = :fk")
                .setParameter("fk", fk)
                .executeUpdate();
    }

    @Transactional
    public void save(Phone phone) {
        if (phone.getId() == null) {
            em.persist(phone);
        } else {
            em.merge(phone);
        }
    }



}
