package dao;

import model.Classifier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ClassifierDao {

    @PersistenceContext
    private EntityManager em;

    public List<Classifier> getAll(){
        return em.createQuery("select c from Classifier c", Classifier.class)
                .getResultList();
    }

    public Long getIdByType(String type){
        Classifier classifier = em.createQuery(
                "select c from Classifier c where c.type = :type", Classifier.class).
                setParameter("type", type).
                getSingleResult();

        return classifier.getId();
    }

}
