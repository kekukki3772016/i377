package dao;

import dto.CustomerDto;
import model.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDao {

    @PersistenceContext
    private EntityManager em;

    @Resource
    PhoneDao pDao;

    public List<Customer> getAll(){
        return em.createQuery("select distinct c from Customer c left join fetch c.phones", Customer.class)
                .getResultList();
    }

    public List<CustomerDto> getAllDto(){
        List<CustomerDto> customerDtoList = new ArrayList<>();
        for (Customer customer : getAll()) {
            customerDtoList.add(new CustomerDto(customer));
        }
        return customerDtoList;
    }

    public Customer getById(Long id){
        return em.createQuery("select distinct c from Customer c left join fetch c.phones where c.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public CustomerDto getDto(Long id){
        return new CustomerDto(getById(id));
    }

    @Transactional
    public void deleteAll(){
        em.createQuery("delete from Customer").executeUpdate();
    }

    @Transactional
    public void deleteById(Long id){
        Customer customer = em.find(Customer.class, id);
        if (customer != null) {
            em.remove(customer);
        }
    }

    @Transactional
    public void save(Customer customer) {

        if (customer.getId() == null) {
            em.persist(customer);
        } else {
            deletePhonesByFk(customer.getId());
            em.merge(customer);
        }
    }

    public void deletePhonesByFk(Long fk){
        em.createQuery("delete from Phone p where p.customer_id = :fk")
                .setParameter("fk", fk)
                .executeUpdate();
    }

    public List<Customer> search(String key) {
        return em.createQuery(
                "select distinct c from Customer c left join fetch c.phones " +
                        "where UPPER(c.firstName) like upper(:key) " +
                        "or UPPER(c.lastName) like upper(:key) " +
                        "or UPPER(c.code) like upper(:key)",
                Customer.class).
                setParameter("key", "%" + key + "%").
                getResultList();
    }
}
