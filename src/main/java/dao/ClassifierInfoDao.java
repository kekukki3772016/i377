package dao;

import dto.ClassifierInfoDto;
import model.ClassifierValue;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ClassifierInfoDao {

    @Resource
    ClassifierDao cDao;

    @Resource
    ClassifierValueDao cvDao;

    public ClassifierInfoDto getAll(){
        return new ClassifierInfoDto(getClassifierInfo("customer_type"), getClassifierInfo("phone_type"));
    }

    private List<String> getClassifierInfo(String type) {

        List<String> classiferInfo = new ArrayList<>();
        StringBuilder sb = new StringBuilder(type);
        Long typeId = cDao.getIdByType(type);
        List<ClassifierValue> classifierValueList = cvDao.getByClassifierId(typeId);

        for (ClassifierValue cv : classifierValueList) {
            classiferInfo.add(String.valueOf(sb.append('.').append(cv.getName())));
            sb = new StringBuilder(type);
        }

        return classiferInfo;
    }


}
