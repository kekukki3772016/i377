package dao;

import model.ClassifierValue;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ClassifierValueDao {

    @PersistenceContext
    private EntityManager em;

    public List<ClassifierValue> getAll(){
        return em.createQuery("select cv from ClassifierValue cv", ClassifierValue.class).getResultList();
    }

    public List<ClassifierValue> getByClassifierId(Long id){
        return em.createQuery(
                "select cv from ClassifierValue cv where cv.classifierId = :id", ClassifierValue.class).
                setParameter("id", id).
                getResultList();
    }

}
